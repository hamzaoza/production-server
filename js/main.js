// Self invoking anonymous function to wrap all code to prevent conflict. 

(function($){

	// Create site object to contain all JS behaviours.

	var site = {
		init : function() {}
	}

	// Call site init function to start JS functionality.

	site.init();

})(jQuery);